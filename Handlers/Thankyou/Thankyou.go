package Login

import (
	"fmt"
	//"github.com/davecgh/go-spew/spew"
	"html/template"
	"log"
	"net/http"
	//DBs "gitlab.com/zendrulat123/flutters/Handlers/DB"
	Handlers "gitlab.com/zendrulat123/flutters/Handlers/Headers"
)

var tpl *template.Template
var err error

func init() {
	tpl = template.Must(template.ParseGlob("templates/*"))
}
func Thankyou(w http.ResponseWriter, r *http.Request) {

	fmt.Println("method:", r.Method) //get request method
	Handlers.Header(w, r)
	switch r.Method {
	case "GET":
		err := tpl.ExecuteTemplate(w, "Thankyou.html", nil)
		if err != nil {
			log.Fatalln("template didn't execute: ", err)
		}

	case "POST":
		fmt.Println(r.Header.Get("Origin"))
		err := tpl.ExecuteTemplate(w, "Thankyou.html", nil)
		if err != nil {
			log.Fatalln("template didn't execute: ", err)
		}

	default:
		fmt.Fprintf(w, "Sorry, only GET and POST methods are supported.")
	}

}
