package Login

import (
	"context"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"html/template"
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/securecookie"
	DBs "gitlab.com/zendrulat123/flutters/Handlers/DB"
	//Handlers "gitlab.com/zendrulat123/flutters/Handlers/Headers"
)

var tpl *template.Template
var err error

type key int

const MyKey key = 0

func init() {
	tpl = template.Must(template.ParseGlob("templates/*"))
}

func SayhelloName(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	spew.Dump(ctx)
	r.ParseForm() //Parse url parameters passed, then parse the response packet for the POST body (request body)
	// attention: If you do not call ParseForm method, the following data can not be obtained form
	fmt.Println(r.Form) // print information on server side.
	fmt.Println("path", r.URL.Path)
	fmt.Println("scheme", r.URL.Scheme)
	fmt.Println(r.Form["url_long"])
	for k, v := range r.Form {
		fmt.Println("key:", k)
		fmt.Println("val:", strings.Join(v, ""))
	}
	Fusername2 := r.Form["username"]
	Fpassword2 := r.Form["password"]
	fmt.Println("Fusername2: ", Fusername2)
	fmt.Println("Fpassword2: ", Fpassword2)

	user := DBs.GetAll() //database user
	fmt.Println("starting matching....")
	for _, v := range user {
		for _, s := range Fpassword2 {
			if v.Password == s {
				fmt.Println(v)
				fmt.Println("matched")
				matched := true
				if matched == true {

					var hashKey = []byte("very-secret")
					var blockKey = []byte("a-lot-secret")
					s := securecookie.New(hashKey, blockKey)

					if cookie, err := r.Cookie("llog"); err == nil {

						value := make(map[string]string)
						if err = s.Decode("llog", cookie.Value, &value); err == nil {
							fmt.Fprintf(w, "The value of foo is %q", value["foo"])

						}
						fmt.Println("cookie looked for")
					} else {
						value := map[string]string{
							"foo": "bar",
						}
						fmt.Println("setting cookie")
						if encoded, err := s.Encode("llog", value); err == nil {
							cookie := &http.Cookie{
								Name:  "llog",
								Value: encoded,
								Path:  "/",
							}
							http.SetCookie(w, cookie)
						}
						fmt.Println(s)
					}

					switch r.Method {
					case "GET":
						err := tpl.ExecuteTemplate(w, "Thankyou.html", nil)
						if err != nil {
							log.Fatalln("template didn't execute: ", err)
						}

					case "POST":
						fmt.Println(r.Header.Get("Origin"))
						err := tpl.ExecuteTemplate(w, "Thankyou.html", nil)
						if err != nil {
							log.Fatalln("template didn't execute: ", err)
						}

					default:
						fmt.Fprintf(w, "Sorry, only GET and POST methods are supported.")
					}
				}
			} else {
				fmt.Println("didnt match")
			}

		}

	}

}

func Login(w http.ResponseWriter, r *http.Request) {
	//fmt.Println("method:", r.Method) //get request method
	//context.Set(r, MyKey, "bar")
	// returns ("bar", true)
	// val, ok := context.GetOk(r, foo.MyKey)
	if r.Method == "GET" {
		err := tpl.ExecuteTemplate(w, "login.html", nil)
		if err != nil {
			log.Fatalln("template didn't execute: ", err)
		}
	} else {

		return
	}
}
